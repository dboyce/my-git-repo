def get_data(dataset):
    # Returns a pandas dataframe object
    import pandas as pd
    df = pd.read_csv(dataset)
    return df

def summary(df):
    return df.describe()
